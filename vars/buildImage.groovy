#!/usr/bin/env groovy

// import the script with the logic of this execution.
import com.example.Docker

// we define a parameterized function whach takes the string parameter imageName.
def call(String imageName) {
    return new Docker(this).buildDockerImage(imageName)
}
