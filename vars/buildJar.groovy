#!/usr/bin/env groovy

def call() {
    // reference to the jenkins global variable BRANCH_NAME.
    echo "building the application jar for branch $BRANCH_NAME ..."
    sh "mvn package"
}
