#!/usr/bin/env groovy

//import the script with the logic of this execution.
import com.example.Docker

//we define a function which returns the docker login logic.
def call() {
    return new Docker(this).dockerLogin()
}
