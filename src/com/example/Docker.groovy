#!/usr/bin/env groovy
package com.example

// create a class with implement serializable
// to support saving the state of the execution
// if the pipeline is paused and resmed.
class Docker implements Serializable {

    // we define a local script variable
    def script

    // we create a parameter "script" which enables the properties
    // of a pipeline (commands, env variables, methods).
    Docker(script) {
        this.script = script
    }

    def buildDockerImage(String imageName) {
        script.echo "building the docker image..."
        script.sh "docker build -t $imageName ."
        
    }

    def dockerLogin() {
        script.withCredentials([script.usernamePassword(credentialsId: 'docker-auth', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
            script.sh "echo $script.PASSWORD | docker login -u $script.USERNAME --password-stdin"
        }
    }

    def dockerPush(String imageName) {
        script.sh "docker push $imageName"
    }
    
}